package com.example.wwandroidsample;

import java.io.File;

import gov.nasa.worldwind.BasicView;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.WorldWindowGLSurfaceView;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.util.dashboard.DashboardView;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class VirtualGlobe extends Activity
{
	static
	{
		System.setProperty( "gov.nasa.worldwind.config.document", "config/myworldwind.xml" );
	}

	protected WorldWindowGLSurfaceView _wwd;
	protected DashboardView _dashboard;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);

		// TODO: temporary method of setting the location of the file store on Android. Replace this with something more flexible.
		File fileDir = getFilesDir();
		System.setProperty( "gov.nasa.worldwind.platform.user.store", fileDir.getAbsolutePath() );

		this.setContentView( R.layout.activity_virtual_globe );

		this._wwd = (WorldWindowGLSurfaceView) this.findViewById(R.id.wwd);
		this._wwd.setModel( (Model) WorldWind.createConfigurationComponent(AVKey.MODEL_CLASS_NAME) );
		this.setupView();
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		// Pause the OpenGL ES rendering thread.
		this._wwd.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		// Resume the OpenGL ES rendering thread.
		this._wwd.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Configure the application's options menu using the XML file res/menu/options.xml.
		this.getMenuInflater().inflate(R.menu.options, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Display the World Wind dashboard when the dashboard options menu item is selected.
		switch( item.getItemId() )
		{
		case R.id.dashboard:
			;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	protected void setupView()
    {
        // TODO: this should be done during View initialization, not here in the application.
        BasicView view = (BasicView) this._wwd.getView();
        Globe globe = this._wwd.getModel().getGlobe();

        Position lookFromPosition = Position.fromDegrees( Configuration.getDoubleValue(AVKey.INITIAL_LATITUDE),
            Configuration.getDoubleValue(AVKey.INITIAL_LONGITUDE),
            Configuration.getDoubleValue(AVKey.INITIAL_ALTITUDE) );
        view.setEyePosition(lookFromPosition, Angle.fromDegrees(0), Angle.fromDegrees(0), globe);
        view.setEyeTilt(Angle.fromDegrees(0), globe);
    }

}
